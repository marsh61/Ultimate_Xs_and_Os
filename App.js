//Writen by Brandon Marshall

var fs = require("fs");
var http = require("http");
var url = require("url");
var mime = require("mime-types");
var qs = require('querystring');
var sql = require('sqlite3').verbose();
var db = new sql.Database("Game_Data.db");
var hat = require('hat');

var server = http.createServer(handleRequest);
var io = require("socket.io")(server);

const ROOT = "./public"


const newBoardData = '*'.repeat(81);
const newBoardTableData = "0".repeat(9);
guestHash ={};

server.listen(3000);
console.log("Listening on port 3000")


function handleRequest(req, res)
{
	console.log("request for" + req.url);
	var urlObj = url.parse(req.url);
	var fileName = ROOT+urlObj.pathname;

	if (req.method == "GET")
	{
		var cookies = parseCookies(req);
		if (((!(cookies.GameID) || !(cookies.PlayerID)) && (urlObj.pathname == "/" || urlObj.pathname == "/MainPage.html")) || urlObj.pathname =="/newGame" )
		{
			fileName = ROOT+"/MainPage.html";
			determineSecondPlayer();

		}
		else if (urlObj.pathname == "/")
		{
			fileName = ROOT+"/MainPage.html";
			fs.stat(fileName,handleStaticFile);
		}
		else{
			fs.stat(fileName,handleStaticFile);
		}
	}
	else
	{
		respond(405, req.method + " is not supported");
	}



	function handleStaticFile(err, stats)
	{
		if (err)
		{
			//file doesn't exist
			respondErr(err);
		}
		else if (stats.isFile())
		{
			fs.readFile(fileName,function(err,data)
			{
				if (err) 
				{
					respondErr(err);
				} else
				{
					respond(200,data);
				}
			});
		}
	}
	function determineSecondPlayer ()
	{
		db.get("SELECT * FROM games WHERE OPlayer = '"+0+"';",function(err,row)
		{
			if (err){
				console.log(err);
                respond(500,"error");
            }
            else if (row == undefined)
            {
            	var newGameID = hat();
				var firstPlayerID = hat();
				buildNewGame(newGameID,firstPlayerID);
            }
            else 
            {
            	var secondPlayerID = hat();
            	insertSecondPlayer(secondPlayerID,row);
            }
		});	
		
	}
	function insertSecondPlayer(secondPlayerID,row)
	{
		db.exec("UPDATE games SET OPlayer ='"+secondPlayerID+"' WHERE ID ='"+row.ID+"';",function(err){
			var cookie = ["GameID="+row.ID+"; expires="+new Date(new Date().getTime()+86409000).toUTCString()+";", "PlayerID="+secondPlayerID+"; expires="+new Date(new Date().getTime()+86409000).toUTCString()+";"];
			fs.readFile(fileName,function(err,data)
			{
				if (err)
				{
					respondErr(err);
				}
				else
				{
					respond(200,data,cookie);
				}
			});
		});

	}
	function buildNewGame(newGameID,firstPlayerID)
	{
		db.get("SELECT ID FROM games WHERE ID ='"+newGameID+"';", checkID);
		
		function checkID (err, row) {
			if (err) {
					console.log(err);
                    respond(500,"error");
            }
			else if (row != undefined)
			{
				newGameID = hat();
				buildNewGame(newGameID,firstPlayerID);
			}
			else 
			{
				storeData(newGameID,firstPlayerID);
			}
		};

		function storeData(newGameID,firstPlayerID)
		{
			db.exec("INSERT INTO games (ID,Turn,XPlayer,OPlayer,GameData,TableData,MinutesRemaining) VALUES ('"+newGameID+"','-','"+firstPlayerID+"','0','"+newBoardData+"','"+newBoardTableData+"','1440');",function(err){
				if (err)
				{
					console.log(err);
                    respond(500,"error");
				}
				else
				{
					var cookie = ["GameID="+newGameID+"; expires="+new Date(new Date().getTime()+86409000).toUTCString()+";", "PlayerID="+firstPlayerID+"; expires="+new Date(new Date().getTime()+86409000).toUTCString()+";"];
					fs.readFile(fileName,function(err,data)
					{
						if (err)
						{
							respondErr(err);
						}
						else
						{
							respond(200,data,cookie);
						}
					});
				}

			});
		}

	}

	function parseCookies(cookies)
	{
		var list = [];
        rc = req.headers.cookie;

   		rc && rc.split(';').forEach(function(cookie) {
        	var parts = cookie.split('=');
        	list[parts.shift().trim()] = decodeURI(parts[0].trim());
    	});

    	return list;
	}

	function respondErr(err)
	{
		console.log("Handle Error");

		respond(500,err.message);

	}

	function respond (code,data,cookie,type)
	{
		res.writeHead(code, {'Set-Cookie': cookie || '',"content-type": type || mime.lookup(urlObj.pathname) || "text/html"});
		res.end(data || "");
	}
}


io.on('connection',function(socket)
{
	console.log("got IO connection");
	initUpdate();

	socket.on('selectTile', function(data){
		console.log(data); 
		var tile = data.tile;
		var cookies = parseCookiesIO(socket);
		console.log(cookies.GameID);
		if (cookies.GameID && cookies.PlayerID)
		{
			db.get("SELECT * FROM games WHERE ID ='"+cookies.GameID+"';", findGame);
		}


		function findGame(err,row)
		{
			if (err) {
					console.log(err);
	            }
				else 
				{
					console.log("i get here0");
					
					if ((cookies.PlayerID == row.XPlayer && row.Turn == '-') && row != undefined)
					{
						updateBoard(row,row.Turn);
					}
					else if ((cookies.PlayerID == row.OPlayer && row.Turn == '+') && row != undefined)
					{
						console.log("i get here1");
						updateBoard(row,row.Turn);
					}
				}
		}

		function updateBoard(row,Turn)
		{
			var gameData = row.GameData;
			var altTurn;
			if (Turn == '-')
				altTurn = '+';
			else if (Turn == '+')
				altTurn = '-';
			console.log('-----------');
			console.log(gameData);
			console.log('-----------');
			if (gameData.charAt(tile) == '*')
			{
				console.log("i get here3");
				gameData = gameData.replace(/\*/g,"0");
				var section = tile % 9;
				gameData = gameData.split("");
				gameData[tile] = Turn;	
				for (var ii = (section*9) ; ii < (section*9+9) ; ii++)
				{
					if (gameData[ii] == 0)
					{
						gameData[ii] = '*';
					}
				}
				var tableData = row.TableData;
				tableData = tableData.split("");
				tableData[section] = checkForTableWin (Turn);
				tableData = tableData.toString();
				tableData = tableData.replace(/,/g,"");
				gameData = gameData.toString()
				gameData = gameData.replace(/,/g,"");
				console.log ("=======");
				console.log (gameData);
				console.log ("=======");
				db.exec("UPDATE games SET GameData ='"+gameData+"', Turn = '"+altTurn+"', TableData = '"+tableData+"' WHERE ID ='"+cookies.GameID+"';",function(err){
					if (err)
					{
						console.log(err);
					}
					else
					{
						if (row != undefined)
						{
							var win = checkForWin();
							var sendObj = {gData: gameData, tData:tableData, turn: Turn, win: win};
							io.emit('update',sendObj);
							sendObj = {gData: gameData, tData:tableData, turn: altTurn, win: win};
							if (Turn == '-')
							{
								if (guestHash[(row.OPlayer).toString()])
								{
									var soc = guestHash[(row.OPlayer).toString()];
									soc.emit('update',sendObj);
								}
							}
							else if (Turn == '+')
							{
								if (guestHash[(row.XPlayer).toString()])
								{
									var soc = guestHash[(row.XPlayer).toString()];
									soc.emit('update',sendObj);
								}
							}
						}
					}
				});
			}
			function checkForWin(Turn)
			{	
				for (var i = 0 ; i < 9 ; i+=3)
				{
					if (tableData[i] == Turn && tableData[i+1] == Turn && tableData[i+2] == Turn)
					{
						return Turn;
					}
				}
				for (var i = 0 ; i < 3 ; i+=1)
				{
					if (tableData[i] == Turn && tableData[i+3] == Turn && tableData[i+6] == Turn)
					{
						return Turn;
					}
				}
				if (tableData[0] == Turn && gameData[4] == Turn && gameData[8] == Turn)
				{
					return Turn;
				}
				if (tableData[2] == Turn && tableData[4] == Turn && tableData[6] == Turn)
				{
					return Turn;
				}
				return 0;
			}
			function checkForTableWin()
			{
				if (tableData[section] == 0)
				{
					for (var i = (section*9) ; i < (section*9+9) ; i+=3)
					{
						if (gameData[i] == Turn && gameData[i+1] == Turn && gameData[i+2] == Turn)
						{
							return Turn;
						}
					}
					for (var i = (section*9) ; i < (section*9+3) ; i+=1)
					{
						if (gameData[i] == Turn && gameData[i+3] == Turn && gameData[i+6] == Turn)
						{
							return Turn;
						}
					}
					if (gameData[(section*9)] == Turn && gameData[(section*9)+4] == Turn && gameData[(section*9)+8] == Turn)
					{
						return Turn;
					}
					if (gameData[(section*9)+2] == Turn && gameData[(section*9)+4] == Turn && gameData[(section*9)+6] == Turn)
					{
						return Turn;
					}
				}
				else
				{
					return tableData[section];
				}
			}
		}
	});

	socket.on("disconnect", function(){
		delete guestHash[parseCookiesIO().PlayerID];
		console.log("disconnected");
	});

	function initUpdate()
	{
		console.log("INITATED UPDATE");
		var cookies = parseCookiesIO(socket);
		guestHash[(cookies.PlayerID).toString()] = socket;
		db.get("SELECT * FROM games WHERE ID = '"+cookies.GameID+"';",function(err,row)
		{
			if (row != undefined)
			{
				var gameData = row.GameData;
				var tableData = row.TableData;
				var turn = 0;
				if (cookies.PlayerID == row.XPlayer)
					turn = '-';
				else if (cookies.PlayerID == row.OPlayer)
					turn = '+';
				var sendObj = {gData: gameData, tData:tableData, turn: turn, win: "0"}
				io.emit('update',sendObj);
			}
		});
	}
	function parseCookiesIO (socket)
	{
		var list = [];
		rc = socket.handshake.headers.cookie;

		rc && rc.split(';').forEach(function(cookie) {
        	var parts = cookie.split('=');
        	list[parts.shift().trim()] = decodeURI(parts[0].trim());
    	});
		return list;
	}

	
});

