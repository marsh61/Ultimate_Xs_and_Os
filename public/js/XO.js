$(document).ready(function(){
	var socket = io();
	var previous = 'green';
	buildBoard();
	function buildBoard()
	{
		var counter = 0;
		var subTableCounter = 0;
		for (var i = 0 ; i < 3; i++)
		{
			var tr = $("<tr></tr>");
			$("#Main-Board").append(tr);

			for (var ii = 0 ; ii < 3 ; ii++)
			{
				var td = $("<td></td>");
				tr.append(td);
				
				var table = $("<table id=T"+subTableCounter+"></table>");
				td.append(table);

				for (var iii = 0 ; iii < 3 ; iii++)
				{
					var tr2 = $("<tr></tr>");
					table.append(tr2);

					for (var iiii = 0; iiii < 3 ; iiii++)
					{
						var td2 = $("<td class='XO_Data' id="+counter+"></td>");
						counter += 1;
						tr2.append(td2);
					}
				}
			}
		}

		$(".XO_Data").on({
			mouseenter: function()
			{
				previous = $(this).css('background-color'); 
				$(this).css('background-color', 'yellow');
			},
			mouseleave: function()
			{
				$(this).css('background-color', previous);
			},
			click: function()
			{
				socket.emit('selectTile',{tile: $(this).attr('id')});
			}

		});
	}

	socket.on('update', function(data)
	{
		var gameData = data.gData.split("");
		var tableData = data.tData.split("");
		var turn = data.turn;
		console.log(data);
		for (var i = 0 ; i < gameData.length ; i++)
		{
			if (gameData[i] == '*')
			{
				$("#"+i).css('background-color','green');
			}
			else if (gameData[i] == '0')
			{
				$("#"+i).css('background-color','#e6e6e6');
			}
			else if (gameData[i] == '-')
			{
				if ('-' == turn)
				{
					$("#"+i).html('X').css('color','#005ce6');
					$("#"+i).css('background-color', '#66a3ff');
					previous = $("#"+i).css('background-color');
				}
				else
				{
					$("#"+i).html('X').css('color','#e60000');
					$("#"+i).css('background-color', '#ff8566');
				}
			}
			else if (gameData[i] == '+')
			{
				if ('+' == turn)
				{
					$("#"+i).html('O').css('color','#005ce6');
					$("#"+i).css('background-color', '#66a3ff');
					previous = $("#"+i).css('background-color');
				}
				else
				{
					$("#"+i).html('O').css('color','#e60000');
					$("#"+i).css('background-color', '#ff8566');
				}
			}
		}
		for (var i = 0 ; i < tableData.length ; i++)
		{
			if (tableData[i] == turn)
			{
				$("#T"+i).css('background-color', '#66a3ff');
			}
			else if (tableData[i] == turn*-1)
			{
				$("#T"+i).css('background-color', '#ff8566');
			}
		}
	});
});